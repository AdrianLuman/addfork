Pinnacle Back-End System Repository

project structure pretty messed up and need refactoring soon

Library dependencies:
- Boost 1.60
- rapidjson

VS2015 Solution folder :
- BackendApplicationProjects
  Solution contains main modules project

- MarketDataProjects
  Solution for development of market data gateway datafeed

Projects:
- ApplicationStarter
  Server app which task is to start backend modules
- CommandGateway
  Module that received command from frontend system that must be executed to backend modules
- ETFProjection
  Module to predict ETF cash component for T+1
- FrontEndGateway (unfinished)
  Module that will handle communication between backend to frontend system
- IpotClient
  Client for Indopremier online broker
- MarketDataPreStarter
  Server app that handle receive and broadcast stock price update
- ModuleMonitoring
  Module that will monitor backend modules and sending monitoring command
- OrderGateway
  Module that manages Broker Client, order and trade data
- PortfolioAdjustment
  Module to adjust historical portfolio data
- PortfolioCashComponent
  Module to calculate cash component of funds for the next trading day
- PortfolioController
  Module that calculate live portfolio data
- PortfolioDividend
  Module that calculate corporate action of funds for the next trading day
- SimasClient
  Client for Sinarmas online broker

Packages:
- Broker Gateway
  package for broker client, order gateway
- comm
  package for application framework, communication framework
- Command Gateway
  package for command gateway
- Market Gateway
  package for market data gateway
- pinnacleLib
  main library for data structure, functions
- Portfolio Controller
  package for portfolio